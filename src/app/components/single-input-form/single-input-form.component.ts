import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {BehaviorSubject, Subject} from "rxjs";

@Component({
  selector: 'app-single-input-form',
  templateUrl: './single-input-form.component.html',
  styleUrls: ['./single-input-form.component.scss']
})
export class SingleInputFormComponent implements OnInit {

  @Input('caption') caption = '';
  @Input('formLabel') formInputLabel = '';
  @Input('inputType') inputType = 'text';
  @Input('buttonText') buttonText = 'Continue';
  @Input('additionalText') additionalText = '';
  @Input('previousButtonHidden') prevButtonHidden = true;

  @Output('next') nextAction = new EventEmitter<boolean>();
  @Output('previous') prevAction = new EventEmitter<boolean>();

  errorSubject = new Subject<string>();
  error$ = this.errorSubject.asObservable();

  constructor() { }

  ngOnInit(): void {
  }

  continue(inputField: HTMLInputElement): void {
    this.errorSubject.next('');
    if (inputField.value.trim()) {
      this.nextAction.emit(true);
    } else {
      this.errorSubject.next('Please enter necessary detail before continuing');
      this.nextAction.emit(false);
    }
  }

  stepBack(inputField: HTMLInputElement): void {
    inputField.value = '';
    this.prevAction.emit(true);
  }
}
