import {Component, Input, OnInit} from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationError, Router} from "@angular/router";
import {MatSidenav} from "@angular/material/sidenav";
import {AuthService} from "../../services/auth.service";
import {BehaviorSubject, Subscription} from "rxjs";
import {UIService} from "../../services/u-i.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @Input('sideNavRef') sidenav: MatSidenav;
  userLoggedIn = false;
  authServiceSub = new Subscription();
  currentPath: any;
  pagePaths = {
    home: '/dashboard',
    activities: '/dashboard/activities',
    help: '/dashboard/help',
    settings: '/dashboard/settings',
    transactions: '/dashboard/transactions',
  }

  constructor(private router: Router,
              private authService: AuthService,
              private uiService: UIService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.authServiceSub = this.authService.userLoggedIn$.subscribe(x => this.userLoggedIn = x);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd || event instanceof NavigationError || event instanceof NavigationCancel) {
        this.currentPath = this.location.path(false);
      }
    });
  }

  navigateToFragment(): void {
    this.router.navigate(['/'], {fragment: 'how_it_works'}).then(() => {
      this.sidenav.close().then(() => this.uiService.Expanded = false);
    })
  }

  navigateToPath(path: string): void {
    this.router.navigate([`${path}`]).then(() => {
      this.sidenav.close().then(() => this.uiService.Expanded = false);
    });
  }

  logOut(): void {
    this.authService.Login = false;
    this.router.navigateByUrl('/').then(() => {
      this.sidenav.close().then(() => this.uiService.Expanded = false);
    });
  }

  onClick(path: string): void {
    this.router.navigateByUrl(`/${path}`).then(() => {
      this.sidenav.close().then(() => this.uiService.Expanded = false);
    });
  }
}
