import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar-auth-pages',
  templateUrl: './navbar-auth-pages.component.html',
  styleUrls: ['./navbar-auth-pages.component.scss']
})
export class NavbarAuthPagesComponent implements OnInit {

  @Input('buttonLabel') buttonLabel = 'sign in';
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToPage(path: string): void {
    if (!path) {
      this.router.navigateByUrl('/');
      return;
    }
    if (path.toLowerCase() === 'sign up') {
      this.router.navigateByUrl('/sign-up');
    }
    if (path.toLowerCase() === 'sign in') {
      this.router.navigateByUrl('/sign-in');
    }
  }
}
