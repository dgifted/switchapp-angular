import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Subscription} from "rxjs";
import {UIService} from "../../services/u-i.service";

@Component({
  selector: 'app-navbar-mobile',
  templateUrl: './navbar-mobile.component.html',
  styleUrls: ['./navbar-mobile.component.scss']
})
export class NavbarMobileComponent implements OnInit, OnDestroy {
  @Output('toggleState') toggle = new EventEmitter<boolean>();
  isExpanded = false;
  userLoggedIn = false;
  authServiceSub = new Subscription();
  uiServiceSub = new Subscription();

  constructor(private authService: AuthService,
              private uiService: UIService) {
  }

  ngOnInit(): void {
    this.authServiceSub = this.authService.userLoggedIn$.subscribe(x => this.userLoggedIn = x);
    this.uiServiceSub = this.uiService.isSidenavExpanded$.subscribe(x => this.isExpanded = x);
    console.log('Is Expanded => ', this.isExpanded);
  }

  toggleExpandedState(): void {
    this.isExpanded = !this.isExpanded;
    this.uiService.Expanded = this.isExpanded;
    this.toggle.emit(this.isExpanded);
  }

  ngOnDestroy(): void {
    this.authServiceSub.unsubscribe();
    this.uiServiceSub.unsubscribe();
  }
}
