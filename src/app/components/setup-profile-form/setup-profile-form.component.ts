import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-setup-profile-form',
  templateUrl: './setup-profile-form.component.html',
  styleUrls: ['./setup-profile-form.component.scss']
})
export class SetupProfileFormComponent implements OnInit {

  @Output('onCancel') onCancel = new EventEmitter<boolean>();

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  cancel(): void {
    this.onCancel.emit(true);
  }

  completedSignUp(): void {
    this.authService.Login = true;
    this.router.navigateByUrl('/dashboard').then();
  }
}
