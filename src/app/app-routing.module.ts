import {NgModule} from '@angular/core';
import {Routes, RouterModule, ExtraOptions} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {SignInComponent} from "./pages/sign-in/sign-in.component";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {SignUpComponent} from "./pages/sign-up/sign-up.component";
import {DashboardHomeComponent} from "./pages/dashboard/dashboard-home/dashboard-home.component";
import {TransactionsComponent} from "./pages/dashboard/transactions/transactions.component";
import {ActivitiesComponent} from "./pages/dashboard/activities/activities.component";
import {SettingsComponent} from "./pages/dashboard/settings/settings.component";
import {HelpComponent} from "./pages/dashboard/help/help.component";
import {AuthGuard} from "./shared/auth.guard";

const routerOptions: ExtraOptions = {
  anchorScrolling: 'enabled',
  scrollPositionRestoration: 'enabled',
  scrollOffset: [0, 64]
};
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'sign-in',
    component: SignInComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', component: DashboardHomeComponent},
      {path: 'transactions', component: TransactionsComponent},
      {path: 'activities', component: ActivitiesComponent},
      {path: 'settings', component: SettingsComponent},
      {path: 'help', component: HelpComponent}
    ]
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes, routerOptions)]
})
export class AppRoutingModule {
}
