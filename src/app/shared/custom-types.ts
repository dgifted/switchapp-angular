export enum ButtonEffects {
  ADD_ICON = 'add_icon',
  REMOVE_ICON = 'remove_icon',
  ADD_BORDER = 'add_border',
  REMOVE_BORDER = 'remove_border'
}
