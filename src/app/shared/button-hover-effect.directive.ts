import {
  Directive,
  ElementRef, HostListener, Input,
  OnChanges,
  Renderer2, SimpleChanges
} from '@angular/core';
import {ButtonEffects} from "./custom-types";

@Directive({
  selector: '[appButtonHoverEffect]'
})
export class ButtonHoverEffectDirective implements OnChanges {

  @Input('icon') icon = '';
  @Input('effect') effect = 'add_icon';
  private readonly className = 'action-button-border';

  constructor(private el: ElementRef,
              private renderer: Renderer2) {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.addEffect(this.effect);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.removeEffect(this.effect);
  }

  private addEffect(effect: string): void {
    if (effect === ButtonEffects.ADD_BORDER) {
      this.setBorder();
    } else if (effect === ButtonEffects.ADD_ICON) {
      this.addIcon();
    }
  }

  private removeEffect(effect: string): void {
    if (effect === ButtonEffects.ADD_BORDER) {
      this.unsetBorder();
    } else if (effect === ButtonEffects.ADD_ICON) {
      this.removeIcon();
    }
  }

  private setBorder(): void {
    this.renderer.addClass(this.el.nativeElement, this.className);
  }

  private unsetBorder(): void {
    this.renderer.removeClass(this.el.nativeElement, this.className)
  }

  private addIcon(): void {
    this.renderer.appendChild(this.el.nativeElement, this.icon);
  }

  private removeIcon(): void {
    this.renderer.removeChild(this.el.nativeElement, this.icon);
  }
}
