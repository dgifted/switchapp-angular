import {Component, OnInit} from '@angular/core';
import {LayoutService} from "../../../services/layout.service";
import {UIService} from "../../../services/u-i.service";
import {Subscription} from "rxjs";
import {AddCashComponent} from "../../../modals/add-cash/add-cash.component";

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss']
})
export class DashboardHomeComponent implements OnInit {
  isMobile = false;
  layoutServiceSub = new Subscription();

  constructor(private layoutService: LayoutService,
              private uiService: UIService) {
  }

  ngOnInit(): void {
    this.layoutServiceSub = this.layoutService.handsetLayout$.subscribe(x => {
      console.log('X => ', x);
      this.isMobile = x.matches;
    });
  }

  openAddCashModal(): void {
    const dialogRef = this.uiService.openDialog(AddCashComponent, this.isMobile);
  }
}
