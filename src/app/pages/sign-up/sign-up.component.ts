import {Component, OnInit} from '@angular/core';
import {signUpStages} from "../../data";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  steps = signUpStages;
  activeStep = this.steps[0];

  constructor() {
  }

  ngOnInit(): void {
  }

  handleNextAction(event: any) {
    if (event) {
      this.incrementActiveStep();
    }
  }

  handlePrevAction(event: any) {
    if (event) {
      this.decrementActiveStep();
    }
  }

  cancelProfileSetup(event: any): void {
    if (event) {
      this.activeStep = this.steps[0];
    }
    return;
  }

  private incrementActiveStep(): void {
    const currentStep = this.steps.indexOf(this.activeStep);
    if (currentStep > -1) {
      console.log('Current Step => ', currentStep);
      if (currentStep < this.steps.length - 1) {
        this.activeStep = this.steps[currentStep + 1];
      } else {
        return;
      }
    }
  }

  private decrementActiveStep(): void {
    const currentStep = this.steps.indexOf(this.activeStep);
    if (currentStep > -1) {
      if (currentStep > 0) {
        this.activeStep = this.steps[currentStep['position'] - 1];
      } else {
        return;
      }
    }
  }
}
