import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  steps = [
    {
      position: 1,
      caption: 'Sign In to your account',
      formLabel: 'Email',
      inputType: 'email',
      buttonText: 'Get verification code'
    },
    {
      position: 2,
      caption: 'Enter the verification code sent to your email.',
      formLabel: '-',
      inputType: 'number',
      buttonText: 'Continue'
    },
    {
      position: 3,
      caption: 'Enter your password',
      formLabel: 'password',
      inputType: 'password',
      buttonText: 'Continue'
    }
  ];

  activeStep = this.steps[0];

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  handleNextAction(event: any) {
    if (event) {
      this.incrementActiveStep();
    }
  }

  handlePrevAction(event: any) {
    if (event) {
      this.decrementActiveStep();
    }
  }


  private incrementActiveStep(): void {
    const currentStep = this.steps.indexOf(this.activeStep);
    if (currentStep > -1) {
      if (currentStep < this.steps.length - 1) {
        this.activeStep = this.steps[currentStep + 1];
      }
      if (currentStep === (this.steps.length - 1)) {
        this.authService.Login = true;
        this.router.navigateByUrl('/dashboard');
      }
    }
  }

  private decrementActiveStep(): void {
    const currentStep = this.steps.indexOf(this.activeStep);
    if (currentStep > -1) {
      if (currentStep > 0) {
        this.activeStep = this.steps[currentStep - 1];
      } else {
        return;
      }
    }
  }
}
