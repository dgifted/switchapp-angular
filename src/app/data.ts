export const debitBankCashAddStages = [
  {
    step: 1,
    headerText: 'How would you like to add cash',
  },
  {
    step: 2,
    headerText: 'How much do you want to add'
  },
  {
    step: 3,
    headerText: 'How would you like to pay'
  },
  {
    step: 4,
    headerText: '',
  }
];

export const signUpStages = [
  {
    position: 1,
    caption: 'Get started with Switch App',
    formLabel: 'Email',
    inputType: 'email',
    buttonText: 'Get verification code'
  },
  {
    position: 2,
    caption: 'Enter the verification code sent to your email.',
    formLabel: '-',
    inputType: 'number',
    buttonText: 'Continue'
  },
  {
    position: 3,
    caption: 'Enter password',
    formLabel: 'password',
    inputType: 'password',
    buttonText: 'Continue'
  },
  {
    position: 4,
    caption: '',
    formLabel: '',
    inputType: '',
    buttonText: ''
  }
];
