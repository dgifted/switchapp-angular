import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UIService} from "../../services/u-i.service";
import {debitBankCashAddStages} from "../../data";

declare let window;

@Component({
  selector: 'app-add-cash',
  templateUrl: './add-cash.component.html',
  styleUrls: ['./add-cash.component.scss']
})
export class AddCashComponent implements OnInit {

  debitBankStages = debitBankCashAddStages;
  currentStage = this.debitBankStages[0];

  constructor(private dialogRef: MatDialogRef<AddCashComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private uiService: UIService) {
  }

  ngOnInit(): void {
  }

  alertMe(): void {
    window.alert('Yeah. Working');
  }

  openNextStage(): void {
    const currentStep = this.debitBankStages.indexOf(this.currentStage);
    if (currentStep > -1) {
      this.currentStage = this.debitBankStages[currentStep + 1];
    }
  }

  closeModal(): void {
    this.dialogRef.close();
  }
}
