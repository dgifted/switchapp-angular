import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCashStepperModalComponent } from './add-cash-stepper-modal.component';

describe('AddCashStepperModalComponent', () => {
  let component: AddCashStepperModalComponent;
  let fixture: ComponentFixture<AddCashStepperModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCashStepperModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCashStepperModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
