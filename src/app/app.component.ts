import {Component, ElementRef, ViewChild} from '@angular/core';
import {MatSidenav} from "@angular/material/sidenav";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(MatSidenav) sidenav: MatSidenav;
  title = 'switch-angular';

  toggleSideNav(event: any): void {
    if (event) {
      this.sidenav.open();
    } else {
      this.sidenav.close();
    }
  }
}
