import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {MatSnackBar, MatSnackBarConfig, MatSnackBarRef} from "@angular/material/snack-bar";
import {MatDialog, MatDialogConfig, MatDialogRef} from "@angular/material/dialog";

@Injectable({
  providedIn: 'root'
})
export class UIService {
  private isBusySubject = new BehaviorSubject<boolean>(false);
  isBusy$ = this.isBusySubject.asObservable();
  private isSidenavExpandedSubject = new BehaviorSubject<boolean>(false);
  isSidenavExpanded$ = this.isSidenavExpandedSubject.asObservable();
  private snackBarConfig: MatSnackBarConfig = {
    duration: 5000,
    horizontalPosition: 'start',
    verticalPosition: 'bottom'
  };

  constructor(private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }


  set busy(value: boolean) {
    this.isBusySubject.next(value);
  }

  set Expanded(value: boolean) {
    this.isSidenavExpandedSubject.next(value);
  }

  openSnackBar(message: string, actionLabel?: string): MatSnackBarRef<any> {
    return this.snackBar.open(message, actionLabel, this.snackBarConfig);
  }

  openDialog(component: any, isMobile = false, data?: any): MatDialogRef<any> {
    const config: MatDialogConfig = {
      data,
      width: isMobile ? '100%' : '30vw',
      closeOnNavigation: true,
    };
    return this.dialog.open(component, config);
  }

  openAddCashStepperDialog(component: any, isMobile = false, data?: any): MatDialogRef<any> {
    const config: MatDialogConfig = {};
    return this.dialog.open(component, config);
  }
}
