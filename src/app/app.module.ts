import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialIndex} from "./material.index";
import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HeroComponent } from './components/hero/hero.component';
import { ActionButtonComponent } from './components/action-button/action-button.component';
import {ExtendedModule, FlexModule} from "@angular/flex-layout";
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { ButtonHoverEffectDirective } from './shared/button-hover-effect.directive';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarAuthPagesComponent } from './components/navbar-auth-pages/navbar-auth-pages.component';
import { SingleInputFormComponent } from './components/single-input-form/single-input-form.component';
import { SetupProfileFormComponent } from './components/setup-profile-form/setup-profile-form.component';
import {FormsModule} from "@angular/forms";
import { SidebarComponent } from './components/dashboard/sidebar/sidebar.component';
import { HeaderComponent } from './components/dashboard/header/header.component';
import { DashboardHomeComponent } from './pages/dashboard/dashboard-home/dashboard-home.component';
import { TransactionsComponent } from './pages/dashboard/transactions/transactions.component';
import { ActivitiesComponent } from './pages/dashboard/activities/activities.component';
import { SettingsComponent } from './pages/dashboard/settings/settings.component';
import { HelpComponent } from './pages/dashboard/help/help.component';
import { AddCashComponent } from './modals/add-cash/add-cash.component';
import { AddCashStepperModalComponent } from './modals/add-cash-stepper-modal/add-cash-stepper-modal.component';
import { NavbarMobileComponent } from './components/navbar-mobile/navbar-mobile.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { FooterMobileComponent } from './components/footer-mobile/footer-mobile.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    HeroComponent,
    ActionButtonComponent,
    SignInComponent,
    DashboardComponent,
    SignUpComponent,
    ButtonHoverEffectDirective,
    FooterComponent,
    NavbarAuthPagesComponent,
    SingleInputFormComponent,
    SetupProfileFormComponent,
    SidebarComponent,
    HeaderComponent,
    DashboardHomeComponent,
    TransactionsComponent,
    ActivitiesComponent,
    SettingsComponent,
    HelpComponent,
    AddCashComponent,
    AddCashStepperModalComponent,
    NavbarMobileComponent,
    SidenavComponent,
    FooterMobileComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialIndex,
        FlexModule,
        FormsModule,
        ExtendedModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
